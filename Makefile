SOURCES=base.c settings.c sokoban.c sokoban2.c solver.c savitch.c solution.c
OBJECTS=$(SOURCES:.c=.o)

DEPS=gtk+-2.0 libpcre sqlite3
override CFLAGS += $(shell pkg-config --cflags $(DEPS))
override LDFLAGS += -lz $(shell pkg-config --static --libs $(DEPS))

PREFIX=/usr

all: usokoban

usokoban: $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o usokoban

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: install
install: usokoban
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp usokoban $(DESTDIR)$(PREFIX)/bin/usokoban
	
	mkdir -p $(DESTDIR)$(PREFIX)/share/usokoban/
	cp -r levels/ $(DESTDIR)$(PREFIX)/share/usokoban/levels/
	cp -r skins/ $(DESTDIR)$(PREFIX)/share/usokoban/skins/

.PHONY: clean
clean:
	rm -f $(OBJECTS)
	rm -f usokoban
